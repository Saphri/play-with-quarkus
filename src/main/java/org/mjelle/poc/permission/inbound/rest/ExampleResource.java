package org.mjelle.poc.permission.inbound.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.annotations.cache.NoCache;

@Path("/api/hello")
@RequestScoped
public class ExampleResource {

    final OidcClient oidcClient;

    ExampleResource(@RestClient OidcClient oidcClient) {
        this.oidcClient = oidcClient;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @NoCache
    public String hello() {
        return oidcClient.getByName();
    }
}