package org.mjelle.poc.permission.inbound.rest;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/default")
@RegisterRestClient
public interface OidcClient {

    @GET
    @Path("/.well-known/openid-configuration")
    String getByName();
}